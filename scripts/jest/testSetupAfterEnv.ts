import { mockI18n, onLog } from '../../src';
import failOnConsole from 'jest-fail-on-console';
import '@testing-library/jest-dom';

/**
 * Importing next during test applies the built-in fetch polyfill by Next.js
 *
 * @see https://github.com/vercel/next.js/discussions/13678#discussioncomment-22383 How to use built-in fetch in tests?
 * @see https://nextjs.org/blog/next-9-4#improved-built-in-fetch-support Next.js Blog - Improved Built-in Fetch Support
 * @see https://jestjs.io/docs/en/configuration#setupfilesafterenv-array About setupFilesAfterEnv usage
 */
require('next');

mockI18n();

failOnConsole();

// fail on logs, if logs are expected, `mockConsoleError` must be used
onLog((data) => console.error(data));
