const config = {
  testEnvironment: 'jsdom',
  rootDir: '../..',
  roots: ['<rootDir>/src'],
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx)$'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'json'],
  setupFiles: ['<rootDir>/scripts/jest/testSetup.ts'],
  setupFilesAfterEnv: [
    '@testing-library/react-hooks/disable-error-filtering.js',
    '<rootDir>/scripts/jest/testSetupAfterEnv.ts',
  ],
  collectCoverageFrom: [
    '**/src/**/*.{ts,tsx}',
    '!**/*.test.{ts,tsx}',
    '!**/*.stories.{ts,tsx}',
    '!**/src/types/**',
  ],
  snapshotSerializers: ['@emotion/jest/serializer'],
  moduleDirectories: ['node_modules', 'src'],
  coveragePathIgnorePatterns: [
    '<rootDir>/src/lib/api/config.ts',
    '<rootDir>/src/lib/testUtils.ts',
    '<rootDir>/src/lib/testId.ts',
    '<rootDir>/src/lib/config.ts',
    '<rootDir>/src/lib/i18n.ts',
    '<rootDir>/.eslintrc.js',
    '<rootDir>/coverage/.*',
  ],
};

// eslint-disable-next-line no-undef
module.exports = config;
