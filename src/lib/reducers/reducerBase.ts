import { AnyActionType } from '../actions/actionTypes';
import { AnyAction } from 'redux';
import produce from 'immer';
import { IdType } from '../../types';

export type BaseReducerState<T extends IdType> = {
  isFetching: boolean;
  isSubmitting: boolean;
  itemList: T[];
};

export function initialBaseReducerState<
  T extends IdType
>(): BaseReducerState<T> {
  return {
    isFetching: false,
    isSubmitting: false,
    itemList: [],
  };
}

interface SagaAction<T extends IdType> extends AnyAction {
  response?: T[] | T;
}

type BaseReducerArgs = {
  load?: AnyActionType;
  del?: AnyActionType;
  update?: AnyActionType;
  add?: AnyActionType;
  clear?: string;
};

export function reducer<T extends IdType>({
  load,
  del,
  update,
  add,
  clear,
}: BaseReducerArgs) {
  return (
    state: BaseReducerState<T> = initialBaseReducerState<T>(),
    action: SagaAction<T>
  ): BaseReducerState<T> =>
    produce(state, (draft: BaseReducerState<T>) => {
      switch (action.type) {
        case load?.request:
          draft.isFetching = true;
          break;
        case load?.success:
          draft.isFetching = false;
          if (action.response) {
            draft.itemList = action.response as T[];
          } else {
            draft.itemList = initialBaseReducerState<T>().itemList;
          }
          break;
        case load?.failure:
          draft.isFetching = false;
          break;
        case del?.request:
          draft.isSubmitting = true;
          break;
        case del?.success: {
          draft.isSubmitting = false;
          const deletedItemId: number = action.requestArgs.id;
          draft.itemList = draft.itemList.filter(
            (item) => item['id'] !== deletedItemId
          );
          break;
        }
        case del?.failure:
          draft.isSubmitting = false;
          break;
        case update?.request:
          draft.isSubmitting = true;
          break;
        case update?.success: {
          draft.isSubmitting = false;
          if (action.response) {
            const updatedItemId: number | undefined = (action.response as T)?.[
              'id'
            ];
            if (updatedItemId !== undefined) {
              const updatedItemIndex = draft.itemList.findIndex(
                (item) => item['id'] === updatedItemId
              );
              draft.itemList[updatedItemIndex] = action.response as T;
            } else {
              console.error("Response doesn't include an id!");
            }
          } else {
            console.error("Response doesn't include updated item!");
          }
          break;
        }
        case update?.failure:
          draft.isSubmitting = false;
          break;
        case add?.request:
          draft.isSubmitting = true;
          break;
        case add?.success: {
          draft.isSubmitting = false;
          if (action.response) {
            draft.itemList.push(action.response as T);
          } else {
            console.error("Response doesn't include added item!");
          }
          break;
        }
        case add?.failure:
          draft.isSubmitting = false;
          break;
        case clear:
          draft.itemList = [];
          break;
      }
    });
}
