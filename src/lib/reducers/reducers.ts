import { combineReducers } from 'redux';
import { toast } from './toast';

export const reducers = combineReducers({
  toast,
});
