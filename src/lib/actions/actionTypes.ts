import { Action } from 'redux';
import { ObjectOf } from '../../types';

type ApiFunction<RequestParams extends ObjectOf<RequestParams>, ResponseType> =
  | ((requestParams: RequestParams) => ResponseType)
  | (() => ResponseType)
  | ((requestParams: RequestParams) => void);

export type ActionType<
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType
> = {
  request: string;
  success: string;
  failure: string;
  apiFunction: ApiFunction<RequestParams, ResponseType>;
};

export type AnyActionType = ActionType<never, unknown>;

export function declareAction<
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType
>(
  name: string,
  apiFunction: ApiFunction<RequestParams, ResponseType>
): ActionType<RequestParams, ResponseType> {
  return {
    request: `${name}_REQUEST` as const,
    success: `${name}_SUCCESS` as const,
    failure: `${name}_FAILURE` as const,
    apiFunction,
  };
}

export const requestAction = <
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType,
  OnSuccessAction extends Action = Action
>(
  actionType: ActionType<RequestParams, ResponseType>,
  requestParams: RequestParams = {} as RequestParams,
  onSuccess?: OnSuccessAction
): Action<typeof actionType.request> & RequestParams => ({
  type: actionType.request,
  ...requestParams,
  onSuccess,
});
