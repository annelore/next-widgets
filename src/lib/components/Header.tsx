import { styled } from '@mui/material/styles';
import React, { ReactElement } from 'react';
import Image from 'next/image';

const Root = styled('div')(({ theme }) => ({
  textAlign: 'right',
  paddingRight: theme.spacing(1),
  paddingTop: theme.spacing(1),
}));

export type HeaderProps = {
  logoSrc: string;
  logoText: string;
  logoWidth: number | string;
  logoHeight: number | string;
};

export const Header = ({
  logoSrc,
  logoText,
  logoWidth,
  logoHeight,
}: HeaderProps): ReactElement => (
  <Root>
    <Image src={logoSrc} width={logoWidth} height={logoHeight} alt={logoText} />
  </Root>
);
