import { caseInsensitive } from './sortTypes';

describe('sortTypes', () => {
  describe('caseInsensitive', () => {
    it.each`
      columnValueA      | columnValueB      | result
      ${''}             | ${''}             | ${0}
      ${''}             | ${'A'}            | ${-1}
      ${'A'}            | ${''}             | ${1}
      ${'A'}            | ${'A'}            | ${0}
      ${'A'}            | ${'a'}            | ${0}
      ${'A'}            | ${'B'}            | ${-1}
      ${'A'}            | ${'b'}            | ${-1}
      ${'a'}            | ${'A'}            | ${0}
      ${'B'}            | ${'A'}            | ${1}
      ${'b'}            | ${'A'}            | ${1}
      ${'Abc'}          | ${'abc'}          | ${0}
      ${'ABC'}          | ${'abc'}          | ${0}
      ${'AbC'}          | ${'aBc'}          | ${0}
      ${'1A'}           | ${'1a'}           | ${0}
      ${'abcdef'}       | ${'ABCDEF'}       | ${0}
      ${'ABCDEF'}       | ${'abcdef'}       | ${0}
      ${'ABCEFG'}       | ${'abcdef'}       | ${1}
      ${'ABCDEF'}       | ${'abcefg'}       | ${-1}
      ${['abc']}        | ${['ABC']}        | ${0}
      ${['b']}          | ${['a']}          | ${1}
      ${['a']}          | ${['B']}          | ${-1}
      ${['abc', 'def']} | ${['ABC', 'DEF']} | ${0}
      ${['ABC', 'DEF']} | ${['abc', 'def']} | ${0}
      ${['ABC', 'EFG']} | ${['abc', 'def']} | ${1}
      ${['ABC', 'DEF']} | ${['abc', 'efg']} | ${-1}
    `(
      'should return "$result" when comparing "$columnValueA" with "$columnValueB"',
      ({ columnValueA, columnValueB, result }) => {
        const columnId = 'someColumnId';
        expect(
          caseInsensitive(
            // prevent having to mock the entire row object
            { values: { [columnId]: columnValueA } } as never,
            { values: { [columnId]: columnValueB } } as never,
            columnId
          )
        ).toEqual(result);
      }
    );
  });
});
