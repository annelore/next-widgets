import { AnyObject } from '../../../types';
import { IdType, Row } from 'react-table';

export const text = <T extends AnyObject>(
  rows: Array<Row<T>>,
  columnIds: Array<IdType<T>>,
  filterValue: string
): Array<Row<T>> => {
  rows = rows.filter((row) => {
    return columnIds.some((id) => {
      const rowValue = row.values[id];
      return String(rowValue)
        .toLowerCase()
        .includes(String(filterValue).toLowerCase());
    });
  });
  return rows;
};
