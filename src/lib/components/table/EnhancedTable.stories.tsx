import { Unpacked } from '../../../types';
import { useMemo } from 'react';
import { IdRequired } from '../../../types/internal/global';
import { EnhancedTable } from './EnhancedTable';
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

const itemList = [
  {
    id: 0,
    name: 'Item 1',
  },
  {
    id: 1,
    name: 'Item 2',
  },
];

type ListItem = Unpacked<typeof itemList>;

const isFirstItem = (item: IdRequired<ListItem>) =>
  item.name === itemList[0].name;

export default {
  title: 'Components/Table/EnhancedTable',
  component: EnhancedTable,
} as ComponentMeta<typeof EnhancedTable>;

const Template: ComponentStory<typeof EnhancedTable> = (args) => {
  const columns = useMemo(
    () => [
      {
        id: 'name',
        Header: 'Name',
        accessor: 'name',
      },
    ],
    []
  );

  return (
    <EnhancedTable<IdRequired<ListItem>>
      {...args}
      columns={columns}
      itemList={itemList}
      isFetching={false}
      isSubmitting={false}
      addButtonLabel={'Custom Item'}
      inline
    />
  );
};

export const Default = Template.bind({});
Default.args = {};

export const WithTitleInline = Template.bind({});
WithTitleInline.args = { title: 'Custom Item', inline: true };

export const CanAdd = Template.bind({});
CanAdd.args = { canAdd: true };

export const CanEdit = Template.bind({});
CanEdit.args = { canEdit: true };

export const CanEditFunction = Template.bind({});
CanEditFunction.args = { canEdit: isFirstItem };

export const CanDelete = Template.bind({});
CanDelete.args = { canDelete: true };

export const CanDeleteFunction = Template.bind({});
CanDeleteFunction.args = { canDelete: isFirstItem };
