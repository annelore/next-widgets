import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import React, { ReactElement } from 'react';
import {
  deleteIconButtonLabelPrefix,
  editIconButtonLabelPrefix,
} from '../../widgets';

export type TableActionButtonsProps = {
  addButtonLabel?: string;
  canEdit?: boolean;
  canDelete?: boolean;
  onEdit?: () => void;
  onDelete?: () => void;
};

export const tableRowButtonLabelGeneric = 'row';

export const TableActionButtons = ({
  addButtonLabel,
  canEdit,
  canDelete,
  onEdit,
  onDelete,
}: TableActionButtonsProps): ReactElement => (
  <div>
    {canEdit && (
      <IconButton
        aria-label={
          editIconButtonLabelPrefix +
          (addButtonLabel ?? tableRowButtonLabelGeneric)
        }
        onClick={onEdit}
        size="large"
      >
        <EditIcon />
      </IconButton>
    )}
    {canDelete && (
      <IconButton
        aria-label={
          deleteIconButtonLabelPrefix +
          (addButtonLabel ?? tableRowButtonLabelGeneric)
        }
        onClick={onDelete}
        size="large"
      >
        <DeleteIcon />
      </IconButton>
    )}
  </div>
);
