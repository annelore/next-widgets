import { ColoredStatus, Status } from './ColoredStatus';
import React from 'react';
import { Meta, Story } from '@storybook/react';

const statuses: Status<boolean>[] = [
  {
    color: 'green',
    value: true,
    text: 'ON',
  },
  {
    color: 'red',
    value: false,
    text: 'OFF',
  },
];

export default {
  title: 'Components/ColoredStatus',
  component: ColoredStatus,
} as Meta<typeof ColoredStatus>;

const Template: Story<typeof ColoredStatus> = (args) => {
  return <ColoredStatus<boolean> value={false} {...args} statuses={statuses} />;
};

export const False = Template.bind({});
False.args = {};

export const True = Template.bind({});
True.args = { value: true };
