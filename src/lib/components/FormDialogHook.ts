import { useState } from 'react';

export interface UseFormDialogHook {
  <T>(selector?: T): {
    openFormDialog: (data?: T) => void;
    data: T | undefined;
    closeFormDialog: () => void;
  };
}

// Return type is defined in `UseFormDialogHook`
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
function useFormDialogFn<F>(initialValue: F) {
  const [data, setData] = useState<F>();
  return {
    openFormDialog:
      initialValue !== undefined
        ? (data = initialValue) => setData(data)
        : setData,
    data,
    closeFormDialog: () => setData(undefined),
  };
}
export const useFormDialog: UseFormDialogHook = useFormDialogFn;
