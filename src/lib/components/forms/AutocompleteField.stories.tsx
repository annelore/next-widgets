import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { AutocompleteField } from './AutocompleteField';
import { FormProvider } from 'react-hook-form';
import { useEnhancedForm } from './hooks';
import { useChoices } from '../choice';

export default {
  title: 'Components/Forms/AutocompleteField',
  component: AutocompleteField,
} as ComponentMeta<typeof AutocompleteField>;

const colors = [
  { name: 'Red', code: 'red' },
  { name: 'Green', code: 'green' },
  { name: 'Blue', code: 'blue' },
];

const Template: ComponentStory<typeof AutocompleteField> = (args) => {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <AutocompleteField
        {...args}
        name="favoriteColor"
        label={'Favorite Color'}
        choices={useChoices(colors, 'code', 'name')}
      />
    </FormProvider>
  );
};

export const Default = Template.bind({});
Default.args = {};
