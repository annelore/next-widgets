import { USERNAME_REGEX } from './validators';

describe('validators', () => {
  describe('username regex', () => {
    it.each`
      input                  | isValid
      ${'CHRISTIANR'}        | ${false}
      ${'foo-bar'}           | ${false}
      ${'chri'}              | ${false}
      ${'christianr'}        | ${true}
      ${'abcdefghijklmnop'}  | ${true}
      ${'abcdefghijklmnopq'} | ${false}
      ${'ululu'}             | ${false}
      ${'ululul'}            | ${true}
    `(
      'should validate the username "$input" as $isValid',
      ({ input, isValid }) => {
        // given
        // when
        const result = USERNAME_REGEX.test(input);
        // then
        expect(result).toEqual(isValid);
      }
    );
  });
});
