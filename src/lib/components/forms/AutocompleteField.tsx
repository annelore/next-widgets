import React, {
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {
  EnhancedArrayFieldProps,
  EnhancedFieldProps,
  useEnhancedArrayField,
  useEnhancedField,
} from './hooks';
import { Choice, ChoiceValue, isChoice } from '../choice';
import { CircularProgress } from '../../widgets/CircularProgress';
import { Controller, FieldValues, UseFormReturn } from 'react-hook-form';
import { Autocomplete } from '@mui/material';
import TextField from '@mui/material/TextField';
import { required } from './validators';
import { SelectFieldBaseProps } from './SelectField';
import { $Values } from 'utility-types';
import { HiddenArrayField, HiddenField } from './HiddenField';

type AutocompleteFieldBaseProps = SelectFieldBaseProps & {
  width?: number | string;
  testId?: string;
  multiple?: boolean;
  setValue: $Values<Pick<UseFormReturn, 'setValue'>>;
  getValues: $Values<Pick<UseFormReturn, 'getValues'>>;
};

function AutocompleteFieldBase(
  props: AutocompleteFieldBaseProps
): ReactElement {
  const {
    name,
    label,
    choices,
    width,
    isLoading,
    testId,
    multiple,
    defaultValue,
    control,
    setValue,
    getValues,
    error,
    disabled,
    ...autocompleteProps
  } = props;
  const [touched, setTouched] = useState<boolean>(false);
  const emptyChoice = useMemo(() => (multiple ? [] : null), [multiple]);

  const defaultChoice = useMemo(() => {
    if (Array.isArray(defaultValue)) {
      return defaultValue
        .map((val) => choices.find((choice) => choice.value === val))
        .filter(isChoice);
    } else {
      return (
        choices.find((choice) => choice.value === defaultValue) ?? emptyChoice
      );
    }
  }, [choices, defaultValue, emptyChoice]);

  const [selectedChoice, setSelectedChoice] = useState<
    Choice | Choice[] | null
  >(defaultChoice);

  const getChoiceValues = useCallback(
    (choice: Choice | Choice[] | null) => {
      let value: ChoiceValue | ChoiceValue[] | null = emptyChoice;
      if (choice) {
        const singleChoiceValue = (choice as Choice).value;
        if (singleChoiceValue) {
          value = singleChoiceValue;
        } else {
          value = (choice as Choice[]).map((c) => c.value);
        }
      }
      return value;
    },
    [emptyChoice]
  );

  useEffect(() => {
    /**
     * `choices` can be loaded asynchronously.
     * If `choices` are empty on first render, even if `defaultValue` is defined,
     * it will result in an empty `defaultChoice` and consequently also `selectedChoice`.
     *
     * As soon as the `choices` were loaded and are not empty anymore,
     * `selectedChoice` needs to be set again.
     *
     * After the user touched the control for the first time,
     * we should not override their choice with the default.
     */
    if (!touched) {
      setSelectedChoice(defaultChoice);
    }
  }, [defaultChoice, touched]);

  useEffect(() => {
    // value in the form state
    const selectedChoiceFormValue: string | undefined = getValues(name);
    // value selected in the component
    const selectedChoiceStateValue =
      getChoiceValues(selectedChoice) ?? undefined;

    /**
     * If the form already has values selected on the first render
     * (e.g. when editing an existing resource), {@link selectedChoiceFormValue} will be defined.
     *
     * In all other cases, {@link selectedChoiceStateValue} will be used as reference,
     * since it is used to set {@link selectedChoiceFormValue} based on it and thus
     * will always be the first value to be up-to-date.
     */
    const selectedChoiceValue =
      selectedChoiceStateValue ?? selectedChoiceFormValue;

    const isSelectedChoiceInChoices = Array.isArray(selectedChoice)
      ? selectedChoice.map((selected) =>
          choices.some((choice) => choice.value === selected.value)
        )
      : choices.some((choice) => choice.value === selectedChoiceValue);

    const isAnyChoiceSelectedInComponent = !!selectedChoiceStateValue;

    if (isAnyChoiceSelectedInComponent && !isSelectedChoiceInChoices) {
      /**
       * When choices are updated, if the component has a choice selected and
       * the selected value is NOT present in the new choices, clear the selection.
       */
      setValue(name, emptyChoice);
      setSelectedChoice(emptyChoice);
    }
  }, [
    choices,
    emptyChoice,
    getChoiceValues,
    getValues,
    name,
    selectedChoice,
    setValue,
  ]);

  if (isLoading) {
    return <CircularProgress />;
  }

  return (
    <Controller
      render={({ field: { onChange, onBlur, ref } }) => (
        <Autocomplete<Choice, boolean>
          ref={disabled ? undefined : ref}
          style={{ width: width ?? 300 }}
          disabled={disabled}
          options={choices}
          onChange={(_e, choice) => {
            if (!touched) {
              setTouched(true);
            }
            setSelectedChoice(choice); // update Autocomplete with whole choice object
            onChange(getChoiceValues(choice)); // update react-hook-forms with only value
          }}
          getOptionLabel={(choice: Choice) => choice?.label}
          isOptionEqualToValue={(option, choice) =>
            option.value === choice.value
          }
          renderInput={(params) => (
            <TextField
              {...params}
              inputProps={{ ...params.inputProps, 'data-testid': testId }}
              label={label}
              error={!!error}
              helperText={error?.message}
              size="small"
            />
          )}
          defaultValue={defaultChoice}
          value={selectedChoice}
          onBlur={onBlur}
          openText={'Open ' + label}
          clearText={'Clear ' + label}
          multiple={!!multiple}
          {...autocompleteProps}
        />
      )}
      defaultValue={defaultValue}
      name={name}
      control={control}
      rules={props.required ? required : undefined}
    />
  );
}

export type AutocompleteFieldProps<
  TInitialValues extends FieldValues = FieldValues
> = EnhancedFieldProps<TInitialValues> &
  Omit<
    AutocompleteFieldBaseProps,
    'defaultValue' | 'error' | 'control' | 'setValue' | 'getValues'
  >;

export function AutocompleteField<
  TInitialValues extends FieldValues = FieldValues
>({
  name,
  initialValues,
  disabled,
  ...props
}: AutocompleteFieldProps<TInitialValues>): ReactElement {
  const enhancedFieldProps = {
    name,
    initialValues,
  };
  const { defaultValue, control, setValue, getValues, error } =
    useEnhancedField<TInitialValues>(enhancedFieldProps);

  return (
    <>
      {disabled && (
        // Add another hidden field when the field is disabled.
        // Otherwise its value becomes 'undefined'
        <HiddenField {...enhancedFieldProps} />
      )}
      <AutocompleteFieldBase
        {...props}
        disabled={disabled}
        name={name}
        defaultValue={defaultValue}
        control={control}
        setValue={setValue}
        getValues={getValues}
        error={error}
      />
    </>
  );
}

export type AutocompleteArrayFieldProps = EnhancedArrayFieldProps &
  Omit<
    AutocompleteFieldBaseProps,
    'error' | 'name' | 'defaultValue' | 'control' | 'getValues' | 'setValue'
  >;

export function AutocompleteArrayField({
  arrayName,
  fieldName,
  field,
  index,
  disabled,
  ...props
}: AutocompleteArrayFieldProps): ReactElement {
  const enhancedFieldProps = {
    arrayName,
    fieldName,
    field,
    index,
  };
  const { error, name, defaultValue, control, setValue, getValues } =
    useEnhancedArrayField(enhancedFieldProps);

  return (
    <>
      {disabled && (
        // Add another hidden field when the field is disabled.
        // Otherwise its value becomes 'undefined'
        <HiddenArrayField {...enhancedFieldProps} />
      )}
      <AutocompleteFieldBase
        {...props}
        error={error}
        name={name}
        disabled={disabled}
        defaultValue={defaultValue}
        control={control}
        setValue={setValue}
        getValues={getValues}
      />
    </>
  );
}
