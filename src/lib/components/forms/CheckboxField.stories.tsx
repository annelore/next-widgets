import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { CheckboxField } from './CheckboxField';
import { FormProvider } from 'react-hook-form';
import { useEnhancedForm } from './hooks';

export default {
  title: 'Components/Forms/CheckboxField',
  component: CheckboxField,
  /*
  Workaround for https://github.com/storybookjs/storybook/issues/12747
  To prevent "Maximum call stack size exceeded" after checking and unchecking
  the checkbox in the "Required" story.
   */
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
    },
  },
} as ComponentMeta<typeof CheckboxField>;

const Template: ComponentStory<typeof CheckboxField> = (args) => {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <CheckboxField {...args} name="accept" label={'I accept'} />
    </FormProvider>
  );
};

export const Empty = Template.bind({});
Empty.args = {};

export const InitialValue = Template.bind({});
InitialValue.args = { initialValues: { accept: true } };

export const Required = Template.bind({});
Required.args = { required: true };

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };
