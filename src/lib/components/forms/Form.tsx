import React, { ReactElement, ReactNode } from 'react';
import { SubmitHandler, useFormContext } from 'react-hook-form';

export type FormProps<T> = {
  onSubmit: SubmitHandler<T>;
  children: ReactNode;
} & Omit<
  React.DetailedHTMLProps<
    React.FormHTMLAttributes<HTMLFormElement>,
    HTMLFormElement
  >,
  'onSubmit'
>;

export function Form<T>({
  onSubmit,
  children,
  ...formProps
}: FormProps<T>): ReactElement {
  const { handleSubmit } = useFormContext<T>();
  return (
    <form onSubmit={handleSubmit<T>(onSubmit)} {...formProps}>
      {children}
    </form>
  );
}
