import React, { ReactElement } from 'react';
import { amber, green, grey, red } from '../widgets/Colors';
import { Typography, TypographyProps } from '../widgets';

type StatusColor = 'amber' | 'green' | 'grey' | 'red';

export interface Status<T> {
  color: StatusColor;
  value: T;
  text?: string;
}

export type ColoredStatusProps<T> = {
  readonly value: T | undefined;
  readonly statuses: Status<T>[];
} & TypographyProps;

export function ColoredStatus<T>({
  value,
  statuses,
  ...typographyProps
}: ColoredStatusProps<T>): ReactElement | null {
  if (value === undefined) {
    return null;
  }
  const status = statuses.find((item) => item.value === value);
  if (!status) {
    return null;
  }

  let color: string;

  switch (status.color) {
    case 'amber':
      color = amber[700];
      break;
    case 'green':
      color = green[500];
      break;
    case 'grey':
      color = grey[600];
      break;
    case 'red':
      color = red[500];
      break;
  }

  return (
    <Typography sx={{ color }} {...typographyProps}>
      {status.text ?? status.value}
    </Typography>
  );
}
