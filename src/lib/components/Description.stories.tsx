import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';
import { Description } from './Description';
import { FormattedItemField } from './list';

export default {
  title: 'Components/Description',
  component: Description,
} as ComponentMeta<typeof Description>;

const Template: ComponentStory<typeof Description> = (args) => (
  <Description {...args} />
);

const entries: FormattedItemField[] = [
  { component: 'Dr.' },
  { caption: 'First Name', component: 'Chuck' },
  { caption: 'Middle Name', component: '' },
  { caption: 'Last Name', component: 'Norris' },
];
const title = 'Registration Form';
const labelWidth = '10%';
const styleRules = {
  caption: {
    padding: 0,
  },
  value: {
    padding: 0,
  },
  row: {
    color: 'red',
  },
  table: {
    width: '100%',
    borderSpacing: 0,
  },
};

export const Default = Template.bind({});
Default.args = { entries, labelWidth };

export const Title = Template.bind({});
Title.args = { ...Default.args, title };

export const StylesRules = Template.bind({});
StylesRules.args = { ...Default.args, styleRules };
