import { getCsrfToken } from '../../api';
import { ListItemButton } from '../list';
import { LogoutIcon } from '../../widgets/icons';
import React, { ReactElement } from 'react';

type LogoutMenuItemProps = {
  logoutUrl: string;
};

export const LogoutMenuItem = ({
  logoutUrl,
}: LogoutMenuItemProps): ReactElement => {
  return (
    <form action={logoutUrl} method="post">
      <input type="hidden" name="csrfmiddlewaretoken" value={getCsrfToken()} />
      <ListItemButton
        value="logout"
        primary="Sign out"
        icon={<LogoutIcon />}
        submit
      />
    </form>
  );
};
