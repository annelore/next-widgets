import React, { ReactElement } from 'react';
import { Drawer } from '@mui/material';

const drawerWidth = 230;

type NavProps = {
  children: React.ReactNode;
};

export const Nav = React.forwardRef<
  HTMLDivElement,
  Omit<NavProps, 'variant' | 'sx'>
>(({ children, ...navProps }, ref): ReactElement => {
  return (
    <Drawer
      variant="permanent"
      ref={ref}
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        '& .MuiPaper-root': {
          color: 'white',
          backgroundColor: '#306278',
          width: drawerWidth,
          borderRightWidth: 0,
          boxShadow:
            'rgba(0, 0, 0, 0.16) 0px 3px 10px, rgba(0, 0, 0, 0.23) 0px 3px 10px',
        },
      }}
      {...navProps}
    >
      {children}
    </Drawer>
  );
});
Nav.displayName = 'Nav';
