import { ListItemLink, ListItemLinkProps } from '../list';
import { styled } from '@mui/material/styles';
import React, { ReactElement } from 'react';

const PREFIX = 'NavItem';

const classes = {
  menuItem: `${PREFIX}-menuItem`,
  menuIconStyles: `${PREFIX}-menuIconStyles`,
};

const selectedColor = '#3f83a4';

const StyledListItemLink = styled(ListItemLink)({
  [`& .${classes.menuItem}`]: {
    color: 'white',
    fontSize: 18,
    textDecoration: 'none',
    textDecorationStyle: 'unset',
  },
  [`& .${classes.menuIconStyles}`]: { color: '#ededed' },
  '&.Mui-selected': {
    backgroundColor: selectedColor,
  },
  '&.Mui-selected:hover': {
    backgroundColor: selectedColor,
  },
});

export const NavItem = ({
  href,
  ...listItemLinkProps
}: ListItemLinkProps): ReactElement => (
  <StyledListItemLink
    {...listItemLinkProps}
    href={href}
    textClasses={{ primary: classes.menuItem }}
    iconClasses={{ root: classes.menuIconStyles }}
  />
);
