import React, { ReactElement } from 'react';
import { Dialog, DialogProps } from '../widgets';
import { Form, FormProps } from './forms';

type FormDialogProps<T> = FormProps<T> & Omit<DialogProps, 'onSubmit'>;

export function FormDialog<T>({
  onSubmit,
  children,
  ...other
}: FormDialogProps<T>): ReactElement {
  return (
    <Dialog
      confirmButtonProps={{
        // make sure submit button is always present (as `onSubmit` is required)
        autoFocus: false,
      }}
      {...other}
    >
      <Form onSubmit={onSubmit} id="dialog-form">
        {children}
      </Form>
    </Dialog>
  );
}
