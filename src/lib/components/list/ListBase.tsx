import React, { ReactElement } from 'react';

import { AddIconButton, ButtonBar, Typography } from '../../widgets';

import { CircularProgress } from '@mui/material';
import { DeleteDialog } from './DeleteDialog';

export type ListBaseProps<T> = {
  itemList: T[];
  itemName?: string;
  deleteItemName?: string;
  canAdd?: boolean;
  emptyMessage?: string;
  children?: React.ReactNode | React.ReactNodeArray;
  inline?: boolean; // if true, the list is not displayed on a full page, but used inline inside of a page
  handleDeleteClose?: (isConfirmed: boolean) => void;
  deleteOpen?: boolean;
  isFetching: boolean;
  isSubmitting: boolean;
  form?: React.ReactNode | null;
  openForm?: () => void;
};

export function ListBase<T>({
  canAdd,
  emptyMessage,
  itemList,
  itemName,
  deleteItemName,
  handleDeleteClose,
  inline,
  children,
  deleteOpen,
  isFetching,
  isSubmitting,
  form: Form,
  openForm,
}: ListBaseProps<T>): ReactElement {
  // To prevent, that onClick gets called with unpredicted arguments propagating to openForm:
  const openAddForm = openForm && (() => openForm());
  const addButton = canAdd && openAddForm && (
    <AddIconButton onClick={openAddForm} itemName={itemName} />
  );
  const isEmpty = !itemList || !itemList.length;

  if (isFetching) {
    return <CircularProgress />;
  }
  return (
    <>
      {isEmpty && emptyMessage ? (
        <Typography component="pre">{emptyMessage}</Typography>
      ) : (
        children
      )}
      {!inline && <ButtonBar>{addButton}</ButtonBar>}
      <DeleteDialog
        open={!!deleteOpen}
        itemName={itemName}
        deleteItemName={deleteItemName}
        isSubmitting={isSubmitting}
        onDelete={(isConfirmed) =>
          handleDeleteClose && handleDeleteClose(isConfirmed)
        }
      />
      {Form}
    </>
  );
}
