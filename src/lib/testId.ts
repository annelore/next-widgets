/**
 * Used to uniquely identify HTML elements in component tests
 * using react-testing-library with `screen.getByTestId(...)`.
 */
export enum TestId {
  TIMELINE_CONNECTOR = 'timeline-connector',
  TIMELINE_DOT_PREFIX = 'timeline-dot-',
}
