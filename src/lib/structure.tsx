import { Action } from 'redux';
import React from 'react';

export type Menu = {
  link: string;
  icon: React.ReactNode;
};

export type StructureItem = {
  title: string;
  menu?: Menu;
};

export type MenuItem = Partial<Menu> & Pick<StructureItem, 'title'>;

export function createMenuItems(structure: StructureItem[]): MenuItem[] {
  return structure
    .map(({ title, menu }) => ({ title, ...menu }))
    .filter((entry) => (entry as Menu).link);
}

export type UserMenuItemModel = {
  title: string;
  icon: React.ReactNode;
  action: Action;
};
