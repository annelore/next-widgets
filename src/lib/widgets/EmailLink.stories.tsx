import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { EmailLink } from './EmailLink';

export default {
  title: 'Widgets/EmailLink',
  component: EmailLink,
} as ComponentMeta<typeof EmailLink>;

const Template: ComponentStory<typeof EmailLink> = (args) => (
  <EmailLink {...args} />
);

const email = 'chuck@norris.gov';
const subject = 'Contact Form';
const body = 'Hi, how are you doing?';

export const Email = Template.bind({});
Email.args = { email };

export const EmailText = Template.bind({});
EmailText.args = {
  ...Email.args,
  children: 'Contact Chuck Norris',
};

export const Subject = Template.bind({});
Subject.args = { ...Email.args, subject };

export const Body = Template.bind({});
Body.args = { ...Email.args, body };

export const SubjectAndBody = Template.bind({});
SubjectAndBody.args = { ...Email.args, ...Subject.args, ...Body.args };
