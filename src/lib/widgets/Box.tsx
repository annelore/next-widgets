import { Box as MuiBox } from '@mui/material';

export * from '@mui/material/Box';
export const Box = MuiBox;
