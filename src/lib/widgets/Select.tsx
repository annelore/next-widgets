import React, { ReactElement, useMemo } from 'react';
import {
  default as MuiSelect,
  SelectProps as MuiSelectProps,
} from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

export type SelectChoice<V> = {
  label: string;
  value: V;
};
export interface SelectProps<V> extends MuiSelectProps {
  choices: SelectChoice<V>[];
  setValue: (value: V) => void;
}

export function Select<Value>({
  choices,
  setValue,
  value,
}: SelectProps<Value>): ReactElement {
  const valueByLabel = useMemo(
    () =>
      choices.reduce((acc: Record<string, Value>, currentValue) => {
        acc[currentValue.label] = currentValue.value;
        return acc;
      }, {}),
    [choices]
  );
  return (
    <MuiSelect
      value={value}
      onChange={(ev) => setValue(valueByLabel[ev.target.value as string])}
    >
      {choices.map(({ label }) => (
        <MenuItem value={label} key={label}>
          {label}
        </MenuItem>
      ))}
    </MuiSelect>
  );
}
