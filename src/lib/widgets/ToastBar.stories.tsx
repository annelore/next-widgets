import React, { useState } from 'react';
import { ComponentMeta, Meta, Story } from '@storybook/react';
import { ToastBar } from './ToastBar';
import { combineReducers, createStore } from 'redux';
import { setToast, Toast, toast, ToastSeverity } from '../reducers';
import { Provider, useDispatch } from 'react-redux';
import { Button } from './Buttons';
import { Unpacked } from '../../types';
import { $Values } from 'utility-types';

const store = createStore(combineReducers({ toast }));

export default {
  title: 'Widgets/ToastBar',
  component: ToastBar,
} as ComponentMeta<typeof ToastBar>;

const Template: Story<Pick<Toast, 'severity'>> = (args) => {
  const [counter, setCounter] = useState<number>(0);
  const dispatch = useDispatch();

  return (
    <div>
      <ToastBar subjectPrefix={'blurb'} contactEmail={'blerb'} />
      <Button
        onClick={() => {
          dispatch(
            setToast({
              message: `Crunchy toast number ${counter} incoming!`,
              ...args,
            })
          );
          setCounter(counter + 1);
        }}
      >
        Make Toast!
      </Button>
    </div>
  );
};

type Decorator = Unpacked<$Values<Pick<Meta, 'decorators'>>>;

const providerDecorator: Decorator = (Story) => (
  <Provider store={store}>
    <Story />
  </Provider>
);

export const Error = Template.bind({});
Error.args = { severity: ToastSeverity.ERROR };
Error.decorators = [providerDecorator];

export const Warning = Template.bind({});
Warning.args = { severity: ToastSeverity.WARNING };
Warning.decorators = [providerDecorator];

export const Info = Template.bind({});
Info.args = { severity: ToastSeverity.INFO };
Info.decorators = [providerDecorator];

export const Success = Template.bind({});
Success.args = { severity: ToastSeverity.SUCCESS };
Success.decorators = [providerDecorator];
