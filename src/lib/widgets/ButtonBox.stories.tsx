import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { AddIconButton, DeleteIconButton, EditIconButton } from './Buttons';
import { ButtonBox } from './ButtonBox';

export default {
  title: 'Widgets/ButtonBox',
  component: ButtonBox,
} as ComponentMeta<typeof ButtonBox>;

const Template: ComponentStory<typeof ButtonBox> = () => {
  return (
    <ButtonBox>
      <AddIconButton />
      <EditIconButton />
      <DeleteIconButton />
    </ButtonBox>
  );
};

export const Default = Template.bind({});
Default.args = {};
