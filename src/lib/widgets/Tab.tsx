import React, { ReactElement, useCallback, useMemo, useState } from 'react';
import { Tab as MuiTab, Tabs as MuiTabs } from '@mui/material';
import { Box } from './Box';
import { Typography } from './Typography';
import isString from 'lodash/isString';

export type TabItem = {
  label: string;
  content: React.ReactNode;
};

function getTabId(id: string, index: number) {
  return `tab-${id}-${index}`;
}

function getTabPanelId(id: string, index: number) {
  return `tabpanel-${id}-${index}`;
}

function a11yProps(id: string, index: number) {
  const tabId = getTabId(id, index);
  return {
    key: tabId,
    id: tabId,
    'aria-controls': getTabPanelId(id, index),
  };
}

export type TabPaneProps = {
  id: string;
  label: string;
  model: TabItem[];
};

export const TabPane = ({ model, label, id }: TabPaneProps): ReactElement => {
  const [selectedTab, setSelectedTab] = useState<number>(0);

  const onTabChange = useCallback(
    (_event: React.SyntheticEvent, newTab: number) => {
      setSelectedTab(newTab);
    },
    []
  );

  const { tabs, tabPanels } = useMemo(() => {
    const tabs: React.ReactNode[] = [];
    const tabPanels: React.ReactNode[] = [];

    for (let i = 0; i < model.length; i++) {
      const tab = model[i];
      tabs.push(<MuiTab label={tab.label} {...a11yProps(id, i)} />);
      tabPanels.push(
        <TabPanel
          key={getTabPanelId(id, i)}
          id={id}
          selectedTab={selectedTab}
          index={i}
        >
          {tab.content}
        </TabPanel>
      );
    }

    return { tabs, tabPanels };
  }, [model, id, selectedTab]);

  return (
    <>
      <MuiTabs
        key={`tab-${id}`}
        indicatorColor="primary"
        textColor="primary"
        value={selectedTab}
        onChange={onTabChange}
        aria-label={label}
      >
        {tabs}
      </MuiTabs>
      {tabPanels}
    </>
  );
};

interface TabPanelProps {
  id: string;
  children?: React.ReactNode;
  index: number;
  selectedTab: number;
}

export function TabPanel({
  id,
  children,
  selectedTab,
  index,
  ...other
}: TabPanelProps): ReactElement {
  const tabPanelId = getTabPanelId(id, index);
  const content = useMemo(
    () =>
      isString(children) ? (
        <Typography key={'typography-' + tabPanelId}>{children}</Typography>
      ) : (
        children
      ),
    [tabPanelId, children]
  );
  return (
    <div
      role="tabpanel"
      hidden={selectedTab !== index}
      key={'div-' + tabPanelId}
      id={tabPanelId}
      aria-labelledby={getTabId(id, index)}
      {...other}
    >
      {selectedTab === index && (
        <Box p={3} key={'box-' + tabPanelId}>
          {content}
        </Box>
      )}
    </div>
  );
}
