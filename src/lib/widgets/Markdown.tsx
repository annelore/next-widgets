import React, { ReactElement, useMemo } from 'react';
import { styled } from '@mui/material/styles';
import snarkdown from 'snarkdown';

const Root = styled('span')(() => ({
  whiteSpace: 'pre-wrap',
}));

export type MarkdownProps = {
  text: string;
  openLinksInNewTab?: boolean;
};

/**
 * NOTE: {@code text} MUST be sanitized and trusted, no input normalization will be performed!
 * Renders {@code text} as markdown.
 * @constructor
 */
export const Markdown = ({
  text,
  openLinksInNewTab,
}: MarkdownProps): ReactElement => {
  const renderedMarkdown = useMemo(() => {
    if (!text) {
      return '';
    }
    const html = snarkdown(text);
    if (openLinksInNewTab) {
      return html.replace(
        new RegExp(/<a href="/g),
        '<a target="_blank" href="'
      );
    }
    return html;
  }, [text, openLinksInNewTab]);
  return <Root dangerouslySetInnerHTML={{ __html: renderedMarkdown }} />;
};
