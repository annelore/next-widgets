import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { PaddingBox } from './PaddingBox';

export default {
  title: 'Widgets/PaddingBox',
  component: PaddingBox,
} as ComponentMeta<typeof PaddingBox>;

const Template: ComponentStory<typeof PaddingBox> = (args) => {
  return <PaddingBox>{args.children}</PaddingBox>;
};

export const Text = Template.bind({});
Text.args = {
  children: <p>Test</p>,
};
