import React, { CSSProperties, ReactElement } from 'react';
import { LinkProps } from 'next/link';
import Fab, { FabProps } from '@mui/material/Fab';

import { blue, brown, green, grey, pink, teal } from './Colors';
import {
  AddIcon,
  ArchiveIcon,
  CreateIcon,
  DeleteIcon,
  DownloadIcon,
  SearchIcon,
  UnarchiveIcon,
} from './icons';
import { Tooltip } from './Tooltip';
import { purple } from '@mui/material/colors';
export { default as Button } from '@mui/material/Button';
export { default as ToggleButton } from '@mui/material/ToggleButton';

type FabStyles = Record<string, FabStyle>;

type FabStyle = {
  icon: React.ElementType;
  buttonStyle: CSSProperties;
  iconStyle?: CSSProperties;
};

const styles: FabStyles = {
  add: { icon: AddIcon, buttonStyle: { backgroundColor: pink[500] } },
  search: { icon: SearchIcon, buttonStyle: { backgroundColor: teal[500] } },
  edit: { icon: CreateIcon, buttonStyle: { backgroundColor: green[400] } },
  del: {
    icon: DeleteIcon,
    buttonStyle: { backgroundColor: grey[200] },
    iconStyle: { fill: grey[500] },
  },
  download: {
    icon: DownloadIcon,
    buttonStyle: { backgroundColor: blue[500] },
  },
  archive: {
    icon: ArchiveIcon,
    buttonStyle: { backgroundColor: brown[500] },
  },
  unarchive: {
    icon: UnarchiveIcon,
    buttonStyle: { backgroundColor: purple[500] },
  },
};

type FabBaseProps = {
  icon: keyof typeof styles;
} & ActionProps;

type ActionProps = Partial<FabProps> &
  Partial<LinkProps> & {
    tooltip?: string;
  };

function FabBase(
  displayName: string,
  actionPropsFactory?: (props: ActionProps) => React.ReactNode
) {
  return Object.assign(
    ({ icon, ...fabProps }: FabBaseProps): ReactElement => {
      const fabStyle = styles[icon];
      const actionProps = actionPropsFactory && actionPropsFactory(fabProps);
      const Icon = fabStyle.icon;
      let FabContent = <Icon sx={fabStyle.iconStyle ?? { fill: 'white' }} />;
      if (fabProps.tooltip) {
        FabContent = <Tooltip title={fabProps.tooltip}>{FabContent}</Tooltip>;
      }
      return (
        <Fab
          sx={{
            marginRight: 'inherit',
            ...fabStyle.buttonStyle,
            '&:hover': { background: 'LightGray' },
          }}
          {...actionProps}
          {...fabProps}
        >
          {FabContent}
        </Fab>
      );
    },
    { displayName }
  );
}

export const FabButton = FabBase('FabButton');

export const addIconButtonLabelPrefix = 'Add ';
export const editIconButtonLabelPrefix = 'Edit ';
export const deleteIconButtonLabelPrefix = 'Delete ';

export const itemNameGeneric = 'item';

export type IconButtonProps = {
  itemName?: string;
} & ActionProps;

export const AddIconButton = ({
  itemName,
  ...actionProps
}: IconButtonProps): ReactElement => {
  const label = addIconButtonLabelPrefix + (itemName ?? itemNameGeneric);
  return (
    <FabButton
      size="small"
      icon="add"
      aria-label={label}
      title={label}
      {...actionProps}
    />
  );
};

export const EditIconButton = ({
  itemName,
  ...actionProps
}: IconButtonProps): ReactElement => {
  const label = editIconButtonLabelPrefix + (itemName ?? itemNameGeneric);
  return (
    <FabButton
      size="small"
      icon="edit"
      aria-label={label}
      title={label}
      {...actionProps}
    />
  );
};

export const DeleteIconButton = ({
  itemName,
  ...actionProps
}: IconButtonProps): ReactElement => {
  const label = deleteIconButtonLabelPrefix + (itemName ?? itemNameGeneric);
  return (
    <FabButton
      size="small"
      icon="del"
      aria-label={label}
      title={label}
      {...actionProps}
    />
  );
};
