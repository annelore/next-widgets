import React, { ReactElement } from 'react';
import MuiAccordion, {
  AccordionProps as MuiAccordionProps,
} from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Divider } from './Divider';

export type AccordionProps = {
  caption: string;
} & MuiAccordionProps;

export function Accordion({
  caption,
  children,
  ...other
}: AccordionProps): ReactElement {
  return (
    <MuiAccordion {...other}>
      <MuiAccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography variant="h6">{caption}</Typography>
      </MuiAccordionSummary>
      <Divider />
      <MuiAccordionDetails>{children}</MuiAccordionDetails>
    </MuiAccordion>
  );
}
