import {
  Step as MuiStep,
  StepConnector as MuiStepConnector,
  stepConnectorClasses,
  StepIconProps,
  StepLabel as MuiStepLabel,
  Stepper as MuiStepper,
  StepperProps,
  StepProps,
} from '@mui/material';
import React, { ReactElement } from 'react';
import { CheckIcon } from './icons';
import { styled } from '@mui/material/styles';

const lightColor = '#eaeaf0';
const accentColor = '#784af4';

const StepConnector = styled(MuiStepConnector)(({ theme }) => ({
  [`&.${stepConnectorClasses.alternativeLabel}`]: {
    top: 10,
    left: 'calc(-50% + 16px)',
    right: 'calc(50% + 16px)',
  },
  [`&.${stepConnectorClasses.active}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: accentColor,
    },
  },
  [`&.${stepConnectorClasses.completed}`]: {
    [`& .${stepConnectorClasses.line}`]: {
      borderColor: accentColor,
    },
  },
  [`& .${stepConnectorClasses.line}`]: {
    borderColor:
      theme.palette.mode === 'dark' ? theme.palette.grey[800] : lightColor,
    borderTopWidth: 3,
    borderRadius: 1,
  },
}));

const StepIconRoot = styled('div')<{ ownerState: { active?: boolean } }>(
  ({ theme, ownerState }) => ({
    color: theme.palette.mode === 'dark' ? theme.palette.grey[700] : lightColor,
    display: 'flex',
    height: 22,
    alignItems: 'center',
    ...(ownerState.active && {
      color: accentColor,
    }),
    '& .StepIcon-completedIcon': {
      color: accentColor,
      zIndex: 1,
      fontSize: 18,
    },
    '& .StepIcon-circle': {
      width: 8,
      height: 8,
      borderRadius: '50%',
      backgroundColor: 'currentColor',
    },
  })
);

const StepIcon = ({ active, completed, className }: StepIconProps) => (
  <StepIconRoot ownerState={{ active }} className={className}>
    {completed ? (
      <CheckIcon className="StepIcon-completedIcon" />
    ) : (
      <div className="StepIcon-circle" />
    )}
  </StepIconRoot>
);

export const Step = ({ children, ...stepProps }: StepProps): ReactElement => (
  <MuiStep {...stepProps}>
    <MuiStepLabel StepIconComponent={StepIcon}>{children}</MuiStepLabel>
  </MuiStep>
);

export const Stepper = ({
  children,
  ...stepperProps
}: StepperProps): ReactElement => {
  return (
    <MuiStepper
      alternativeLabel
      connector={<StepConnector />}
      {...stepperProps}
    >
      {children}
    </MuiStepper>
  );
};
