import { Typography as MuiTypography, TypographyProps } from '@mui/material';
import React, { ReactElement } from 'react';

export * from '@mui/material/Typography';
export const Typography: typeof MuiTypography = (
  props: TypographyProps
): ReactElement => <MuiTypography variant={'body2'} {...props} />;
