import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { FabButton } from './Buttons';

export default {
  title: 'Widgets/FabButton',
  component: FabButton,
} as ComponentMeta<typeof FabButton>;

const Template: ComponentStory<typeof FabButton> = (args) => {
  return (
    <FabButton
      {...args}
      icon="download"
      tooltip="Download"
      // eslint-disable-next-line no-console
      onClick={(): void => console.log('Download')}
    />
  );
};

export const Default = Template.bind({});
Default.args = {};

export const Small = Template.bind({});
Small.args = { size: 'small' };
