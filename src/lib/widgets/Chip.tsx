import React, { ReactElement, ReactNode } from 'react';
import MaterialUiChip, {
  ChipProps as MaterialUiChipProps,
} from '@mui/material/Chip';

import Avatar from '@mui/material/Avatar';
import { Tooltip } from './Tooltip';
import { FaceIcon } from './icons';

type ChipProps = {
  icon?: ReactNode;
  children?: null;
} & MaterialUiChipProps;

export const Chip = React.forwardRef<HTMLDivElement, ChipProps>(
  ({ icon, ...other }, ref) => (
    <MaterialUiChip
      style={{ marginBottom: 4 }}
      avatar={<Avatar>{icon}</Avatar>}
      ref={ref}
      {...other}
    />
  )
);
Chip.displayName = 'Chip';

type ChipWithTooltipProps = {
  tooltip?: string | null;
} & ChipProps;

export const ChipWithTooltip = ({
  tooltip,
  ...other
}: ChipWithTooltipProps): ReactElement => {
  if (tooltip) {
    return <Tooltip title={tooltip}>{<Chip {...other} />}</Tooltip>;
  } else {
    return <Chip {...other} />;
  }
};

type UserChipProps = {
  user: string | undefined;
} & ChipWithTooltipProps;

export const UserChip = ({ user, ...props }: UserChipProps): ReactElement => (
  <ChipWithTooltip icon={<FaceIcon />} label={user} {...props} />
);
