import { isClient, isServer } from '../utils';
import Cookies from 'js-cookie';

/**
 * Can be used to redirect to an external {@code url}.
 * To redirect to internal URL's, use push from {@code const { push } = useRouter()} instead.
 */
export function redirectExternal(url: string): void {
  if (isClient()) {
    window.location.href = url;
  }
}

export function getCookie(name: string): string {
  if (isServer()) {
    return '';
  }
  if (!document.cookie) {
    console.error('Cannot retrieve cookies!');
    return '';
  }
  return Cookies.get(name) || '';
}

export function csrfHeader(): Record<'X-CSRFToken', string> {
  return { 'X-CSRFToken': getCsrfToken() };
}

export function getCsrfToken(): string {
  return getCookie('csrftoken');
}
