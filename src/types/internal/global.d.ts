export type IdRequired<T> = Required<T, 'id'>;
