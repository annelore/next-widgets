<h1 align="center">next-widgets</h1>

<p align="center">Commonly used widgets, building blocks for forms, table components and utilities for Next.js.</p>

<p align="center">
    <a href="https://www.npmjs.com/package/@biomedit/next-widgets"><img src="https://img.shields.io/npm/v/@biomedit/next-widgets/latest.svg?style=flat-square" alt="NPM Version" /></a>
    <a href="https://www.npmjs.com/package/@biomedit/next-widgets"><img src="https://img.shields.io/npm/dm/@biomedit/next-widgets.svg?style=flat-square" alt="NPM Downloads" /></a>
    <a href="https://master--6169af96fe171f004a2c1224.chromatic.com"><img src="https://img.shields.io/badge/docs-storybook-blue?style=flat-square" alt="Storybook Documentation" /></a>
    <a href="https://gitlab.com/biomedit/next-widgets/-/pipelines"><img src="https://img.shields.io/gitlab/pipeline/biomedit/next-widgets/master?style=flat-square" alt="GitLab CI Build Status" /></a>
    <a href="https://gitlab.com/biomedit/next-widgets/-/blob/master/LICENSE"><img src="https://img.shields.io/npm/l/@biomedit/next-widgets?style=flat-square" alt="GitLab license" /></a>
</p>

## Install

This project follows the [semantic versioning specification](https://semver.org/) for its releases.

To install, run the following command:

```
npm install @biomedit/next-widgets
```

Additionally, you need to have the following dependencies and versions installed in your application:

```
"@emotion/react": "11.x",
"@emotion/styled": "11.x",
"@mui/icons-material": "5.x",
"@mui/lab": "^5.0.0-alpha.45",
"@mui/material": "5.x",
"next": "11.x",
"react": "17.x",
"react-dom": "17.x",
"react-hook-form": "7.x",
"react-redux": "7.x",
"redux": "4.x",
"redux-saga": "1.x"
```

## Usage

### Logging

To get logs from `next-widgets`, use the `onLog` and `offLog` methods to register and unregister a listener, respectively.
For example, to log everything to the console, add the following to `_app.tsx`:

```ts
import { LogListener, onLog, offLog } from '@biomedit/next-widgets';

useEffect(() => {
  const listener: LogListener = (data) => {
    console.log(data);
  };
  onLog(listener);
  return () => {
    offLog(listener);
  };
}, []);
```

To use the logger from `next-widgets` in your application, pass a namespace to the `logger` function to get a logger.
Then, you can call the log levels as methods on that logger:

```ts
import { logger } from '@biomedit/next-widgets';

const log = logger('Example');

log.silly(message);
log.verbose(message);
log.info(message);
log.http(message);
log.warn(message);
log.error(message);
```

### ToastBar

To use the `ToastBar`, first add the component to your `_app.tsx`:

```tsx
function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <ToastBar
        subjectPrefix={'subjectPrefix'}
        contactEmail={'chuck@norris.gov'}
      />
      <Component {...pageProps} />
    </>
  );
}
```

Then, add the `toast` reducer to your reducers in `redux`:

```ts
import { toast } from '@biomedit/next-widgets';

export const reducers = combineReducers({
  ...,
  toast,
});
```

Finally, add a typing declaration file with the following content (for example `next-widgets.d.ts`), replacing
`RootState` with the type of your redux store's state:

```
import 'next-widgets';
import { RootState } from './store';
declare module 'next-widgets' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultRootState extends RootState {}
}
```

### Customizations

The unique validation that is performed in `LabelledField` when the `unique` property is specified performs debouncing,
which is set to **750 ms** by default.
To change the debouncing time, set the environment variable `NEXT_PUBLIC_UNIQUE_VALIDATION_DEBOUNCE` to the desired
amount of milliseconds.

## Generate Typing Documentation

To generate typing documentation, clone this repository locally, then run the following commands:

```
npm install
npm run doc
```

Finally, open the `build/docs/index.html` file in your browser.

## Development

### Requirements

- NodeJS >=16

### Install

1. Clone this repository locally
1. Run `npm install`

### Running tests

Run: `npm test`

This will do the following:

- Build the source files
- Run ESLint (check only)
- Run prettier (check only)
- Run unit tests using jest

### Running tests in watch mode

Run: `npm run watch`

### Automatically fix eslint and reformat using prettier

To automatically fix errors by eslint (limited to those that can be fixed automatically) and have the code get
reformatted using prettier, run the following command:

```
npm run fix
```

### Try out changes locally

There are two ways to try out your changes locally, you can either use Storybook, or you can build and publish an npm
package to a local registry, so you can install it as a dependency in a different NodeJS application.

Storybook is recommended, as it's the fastest way to try out changes locally.

In some special cases it might be necessary to publish a package, but this will require you to build the source files
every time you make a change.

#### Storybook

Run storybook using the following command:

```
npm run storybook
```

After building the storybook, it will automatically open it in your browser.

If the component you want to try out doesn't exist yet, you need to create a `.stories.tsx` file in the same folder
as the component.

After making changes to either the stories or the source files, storybook will automatically rebuild and refresh
upon saving, so there is no need to refresh the page or rerun the command.

#### Publishing a package locally

##### First time setup

Run the following commands to install and run a local npm registry:

```
npm install -g verdaccio
verdaccio
```

Add a user with the following command, remembering the credentials you defined for later:

```
npm adduser --registry http://localhost:4873/
```

##### Publishing to the local npm registry

1. Make sure the local npm registry is running, by running `verdaccio` in a separate terminal window.
1. Set the current registry of npm to your local one: `npm set registry http://localhost:4873/`
1. Change the version number in `package.json` to one that doesn't exist yet.
1. Run `npm run build` (or `npm run watch:build` if you plan on making further changes)
1. Run `npm publish` (you may need to enter in the credentials as specified in the initial setup)
1. In your application, change the version of `@biomedit/next-widgets` in your `package.json` which you defined in a
   previous step and run `npm i`
1. To change back to the original npm registry, run `npm config set registry https://registry.npmjs.org/`

Make sure you don't commit your `package-lock.json` with your npm registry set to your local registry, always make sure
to change back to the original npm registry first and run `npm i`.
It's always a good idea to search the `package-lock.json` for `localhost:4873` to make sure you don't accidentally
include anything from a local registry.
